# AnetA8-SKR-Mini-v2-Marlin-Config

My configuration files for the BigTreeTech SKR E3 Mini v2 on the Anet A8

**Marlin v2.0.7.2**

* Capacitive Probe
* Zonestar LCD (stock)

**CAUTION** You need to rewire the display or you will damage your board